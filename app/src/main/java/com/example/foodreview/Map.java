package com.example.foodreview;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

public class Map extends AppCompatActivity implements OnMapReadyCallback {
    private String TAG = "Màn hình tìm kiếm";
    private String apiKey = "AIzaSyBt1Rrh8_AAfqTFHXJzFArPqBIquoyNLUA";
    private TextView place_search;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    List<Place.Field> fields;
    String latitude = "", longtitude = "";
    private GoogleMap mMap;
    Place place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        place_search = findViewById(R.id.place_Search);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

            PlacesClient placesClient = Places.createClient(this);
            fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
            place_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                            .build(Map.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                }
            });


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(10.775676,106.700226);
        LatLng sydney2 = new LatLng(10.788247,106.704705);
        mMap.addMarker(new MarkerOptions()
                .position(sydney)
                .title("Quan an  Hutech 1"))
                .setSnippet("Dia chi quan an 1...");
        mMap.addMarker(new MarkerOptions()
                .position(sydney2)
                .title("Quan an  Hutech 2"))
                .setSnippet("Dia chi quan an 2...");
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.setMinZoomPreference(6.0f);
        mMap.setMinZoomPreference(15.0f);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                place_search.setText(place.getAddress());
                longtitude= String.valueOf(place.getLatLng().longitude);
                latitude= String.valueOf(place.getLatLng().latitude);
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;
        }

    }



}